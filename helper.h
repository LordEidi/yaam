/*-----------------------------------------------------------------------------
 **
 ** - yaam -
 **
 ** Yet Another ArchiveMail
 **
 ** Copyright 2023 by SwordLord - the coding crew - http://www.swordlord.com
 ** and contributing authors
 **
 ** This program is free software; you can redistribute it and/or modify it
 ** under the terms of the GNU Affero General Public License as published by the
 ** Free Software Foundation, either version 3 of the License, or (at your option)
 ** any later version.
 **
 ** This program is distributed in the hope that it will be useful, but WITHOUT
 ** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 ** FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 ** for more details.
 **
 ** You should have received a copy of the GNU Affero General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 **-----------------------------------------------------------------------------
 **
 ** Original Authors:
 ** LordEidi@swordlord.com
 **
-----------------------------------------------------------------------------*/
#ifndef YAAM_HELPER_H
#define YAAM_HELPER_H

#include <string>
#include <filesystem>
#include <iostream>

#include <fcntl.h>

#include <archive.h>
#include <archive_entry.h>
#include <vmime/vmime.hpp>

auto readFile(std::string_view path) -> std::string;

void replaceAll(std::string& input, const std::string& from, const std::string& to);

void writeArchive(std::filesystem::path& out, std::filesystem::path& in, std::string& filename);

namespace yaam
{
    // class to be used to write all IMAP traffic to stdout, only used when using --debug-imap
    class yaamIMAPTracer : public vmime::net::tracer
    {
    public:

        yaamIMAPTracer(
                const vmime::shared_ptr <std::ostringstream>& stream,
                const vmime::shared_ptr <vmime::net::service>& serv,
                const int connectionId
        )
                : m_stream(stream),
                  m_service(serv),
                  m_connectionId(connectionId) {

        }

        void traceSend(const vmime::string& line)
        {
            // TODO switch back to the stream which was given during the construction
            std::cout << "[" << m_service->getProtocolName() << ":" << m_connectionId << "] C: " << line << std::endl;
            //*m_stream << "[" << m_service->getProtocolName() << ":" << m_connectionId << "] C: " << line << std::endl;
        }

        void traceReceive(const vmime::string& line)
        {
            // TODO switch back to the stream which was given during the construction
            std::cout << "[" << m_service->getProtocolName() << ":" << m_connectionId << "] S: " << line << std::endl;
            //*m_stream << "[" << m_service->getProtocolName() << ":" << m_connectionId << "] S: " << line << std::endl;
        }

    private:

        vmime::shared_ptr <std::ostringstream> m_stream;
        vmime::shared_ptr <vmime::net::service> m_service;
        const int m_connectionId;
    };

    // used together with the IMAPTracer
    class IMAPTracerFactory : public vmime::net::tracerFactory
    {
    public:

        explicit IMAPTracerFactory(const vmime::shared_ptr <std::ostringstream>& stream) : m_stream(stream) {

        }

        vmime::shared_ptr <vmime::net::tracer> create(
                const vmime::shared_ptr <vmime::net::service> serv,
                const int connectionId
        )  {

            return vmime::make_shared <yaamIMAPTracer>(m_stream, serv, connectionId);
        }

    private:

        const vmime::shared_ptr <std::ostringstream>& m_stream;
    };

    // Warning, this currently just ignores everything which would look like a validation function and just accepts every certificate...
    class selfSignedCertVerifier : public vmime::security::cert::certificateVerifier
    {
    public:
        void verify (vmime::shared_ptr<vmime::security::cert::certificateChain> certs, const std::string& hostname)
        {
            // this will allways return "true" and accept every cert w/o checking!
            // dev use only
            /*
            throw vmime: : security : : cert : : certificateException ( ) ;
             */
        }
    };

}
#endif //YAAM_HELPER_H
