/*-----------------------------------------------------------------------------
 **
 ** - yaam -
 **
 ** Yet Another ArchiveMail
 **
 ** Copyright 2023 by SwordLord - the coding crew - http://www.swordlord.com
 ** and contributing authors
 **
 ** This program is free software; you can redistribute it and/or modify it
 ** under the terms of the GNU Affero General Public License as published by the
 ** Free Software Foundation, either version 3 of the License, or (at your option)
 ** any later version.
 **
 ** This program is distributed in the hope that it will be useful, but WITHOUT
 ** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 ** FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 ** for more details.
 **
 ** You should have received a copy of the GNU Affero General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 **-----------------------------------------------------------------------------
 **
 ** Original Authors:
 ** LordEidi@swordlord.com
 **
-----------------------------------------------------------------------------*/

#include "date.h"
#include "archiver.h"
#include "helper.h"
#include "log.h"

#include <re2/re2.h>

void archiveMailsource(std::string& ms)
{
    // check what kind of mailsource is given and decide what backup logic to roll...
    if (ms.starts_with("imap"))
    {
        std::string protocol;
        std::string secure;
        std::string uid;
        std::string pwd;
        std::string server;
        std::string folder;

        // To archive IMAP mailboxes, format your mailbox argument like this:
        //  imap://username:password@server/mailbox
        //  (substitute 'imap' with 'imaps' for an SSL connection)
        //RE2 re("(?P<protocol>imap)(?P<secure>s{1})?(?::\\/\\/)(?P<uid>[a-zA-Z0-9._]*)(?:(?::)(?P<pwd>[a-zA-Z0-9,:;_\\^<>=/()!.-]*))?(?:@)(?P<server>[a-zA-Z0-9._]*)(?:(?:\\/)(?P<folder>[a-zA-Z0-9._]*))+");
        RE2 re("(?P<protocol>imap)(?P<secure>s{1})?(?::\\/\\/)(?P<uid>[a-zA-Z0-9._]*)(?:(?::)(?P<pwd>[a-zA-Z0-9!-\\/:-@[-`{-~]*))?(?:@)(?P<server>[a-zA-Z0-9._]*)(?:(?:\\/)(?P<folder>[a-zA-Z0-9._/]*))+");
        assert(re.ok());  // compiled; if not, see re.error();

        if(!RE2::FullMatch(ms, re, &protocol, &secure, &uid, &pwd, &server, &folder))
        {
            error("an error occured during parsing the imap connection string {} {}", ms, re.error());
            return;
        }

        if(secure.length() > 0)
        {
            protocol += secure;
        }

        info("using imap connection: {}", ms);

        // 	url(const string& protocol, const string& host, const port_t port = UNSPECIFIED_PORT,
        //		const string& path = "", const string& username = "", const string& password = "");
        vmime::utility::url url(protocol, server);
        url.setPath(folder);

        // TODO switch to this somewhen? Not yet, since it does not work, or does not work with SASL
        // class myAuthenticator : public vmime::security::defaultAuthenticator
        // sess->getProperties() ["store.imap.auth.username"] = uid;
        url.setUsername(uid);

        // get the password depending of params
        if(conf.imap_pwd.length() > 0)
        {
            //sess->getProperties() ["store.imap.auth.password"] = conf.imap_pwd;
            url.setPassword(conf.imap_pwd);
        }
        else if(pwd.length() > 0)
        {
            //sess->getProperties() ["store.imap.auth.password"] = pwd;
            url.setPassword(pwd);
        }

        std::string s_folder_name{folder};
        std::replace( s_folder_name.begin(), s_folder_name.end(), '/', '.');

        archiveMails(ms, s_folder_name, url);
    }
    else
    {
        std::filesystem::path p_mailsource_folder;
        try
        {
            p_mailsource_folder = std::filesystem::canonical(ms);
        }
        catch(const std::exception& ex)
        {
            error("parsing mailbox path for {} threw an exception: {}. Stopping execution for this path.", ms, ex.what());
            return;
        }

        std::string s_mailsource_folder = p_mailsource_folder.string();

        std::string sURL{"maildir://localhost" + s_mailsource_folder};
        info("opening session to mail store: {}", sURL);

        vmime::utility::url url("maildir://localhost" + s_mailsource_folder);

        std::filesystem::path p_maildir_source{ms};
        std::string s_folder_name{p_maildir_source.parent_path().filename()};

        archiveMails(ms, s_folder_name, url);
    }
}

void archiveMails(std::string& ms, std::string& target_mailbox_name, vmime::utility::url& url)
{
    std::string sURL{ms};
    info("opening session to mail store: {}", sURL);

    vmime::shared_ptr<vmime::net::store> st = sess->getStore(url);
    if(st == nullptr)
    {
        warning("an error happened while trying to get mail store: {}", ms);
        return;
    }

    if(conf.debug_imap)
    {
        // append tracer to see communication
        vmime::shared_ptr<std::ostringstream> traceStream = vmime::make_shared <std::ostringstream>();
        st->setTracerFactory(vmime::make_shared<yaam::IMAPTracerFactory>(traceStream));
    }

    //vmime::shared_ptr <vmime::net::tls::TLSProperties> tlsProps = vmime::make_shared<vmime::net::tls::TLSProperties>();
    // TODO fix this self signed exception

    // TODO check what standard verifier does. Do we need to implement it on our own?
    if(conf.no_tls_warning)
    {
        st->setCertificateVerifier(vmime::make_shared<yaam::selfSignedCertVerifier>());
    }

    st->connect();
    if(!st->isConnected())
    {
        warning("an error happened while trying to connect to mail store: {}", ms);
        return;
    }

    vmime::net::folder::path fldp;

    if(url.getProtocol().starts_with("imap"))
    {
        // have vmime parse the URL, otherwise vmime thinks the whole path is a single component
        fldp = vmime::net::folder::path::fromString(url.getPath(),"/", vmime::charset::getLocalCharset());
    }
    else
    {
        // this is a hack for maildir, but this is the only path vmime accepts for the current folder
        fldp = vmime::net::folder::path(".");
    }

    vmime::shared_ptr<vmime::net::folder> fld = st->getFolder(fldp);
    if(!fld->exists())
    {
        warning("Folder does not exist: {}", url.getPath());
        return;
    }

    std::string s_target_mbox_filename{};

    if(conf.prefix_formatted.length() > 0)
    {
        s_target_mbox_filename += conf.prefix_formatted;
    }

    // remove the leading . if there is one. It will otherwise lead to archive names beginning
    // with a dot (which are then hidden in the folder view)
    if(target_mailbox_name.starts_with("."))
    {
        s_target_mbox_filename += target_mailbox_name.substr(1, target_mailbox_name.length() - 1);
    }
    else
    {
        s_target_mbox_filename += target_mailbox_name;
    }

    if(conf.suffix_formatted.length() > 0)
    {
        s_target_mbox_filename += conf.suffix_formatted;
    }

    // hard coded suffix for the file containing the archived mails
    s_target_mbox_filename += ".mbox";

    std::filesystem::path p_mbox_target{conf.output_dir};
    p_mbox_target /= s_target_mbox_filename;

    info("using mbox file: {}", p_mbox_target.string());

    if(std::filesystem::exists(p_mbox_target))
    {
        warning("mbox file '{}' already exists, stopping processing of mailbox.", p_mbox_target.string());
        return;
    }

    std::ofstream* os_mbox_target = new std::ofstream();

    // don't open file if --dry-run is set
    if(!conf.dry_run)
    {
        os_mbox_target->open(p_mbox_target, std::ios_base::app);
    }

    vmime::shared_ptr<vmime::net::folderStatus> fs = fld->getStatus();

    info("processing folder '{}'", fld->getFullPath().toString("/", vmime::charset::getLocalCharset()));

    vmime::size_t count, unseen;
    fld->status(count, unseen);
    info("{} mails found in total, of which {} are unread.", count, unseen);

    // nothing to output if folders are empty...
    if(count == 0)
    {
        info("nothing to be done, ending processing of '{}'", fld->getFullPath().toString("/", vmime::charset::getLocalCharset()));

        // TODO: put this into a helper function (and replace the code at the end of this with the new function as well
        if(os_mbox_target->is_open())
        {
            os_mbox_target->close();
            std::filesystem::remove(p_mbox_target);
        }
        return;
    }

    uint16_t total_mail_count{static_cast<uint16_t>(count)};
    uint16_t archived_mail_count{0};

    if(conf.dry_run)
    {
        fld->open(vmime::net::folder::MODE_READ_ONLY);
    }
    else
    {
        fld->open(vmime::net::folder::MODE_READ_WRITE);
    }

    // archiving starting here -----------------------------------------------------------------------------------------------------
    std::chrono::system_clock::time_point tp_today = floor<date::days>(std::chrono::system_clock::now());

    std::vector<vmime::shared_ptr<vmime::net::message>> all_messages = fld->getMessages(vmime::net::messageSet::byNumber(1 , -1));
    fld->fetchMessages(all_messages, vmime::net::fetchAttributes::FLAGS | vmime::net::fetchAttributes::ENVELOPE | vmime::net::fetchAttributes::SIZE);
    for (uint64_t i = 0 ; i < all_messages.size () ; ++i )
    {
        vmime::shared_ptr<vmime::net::message> msg = all_messages[i] ;
        const int flags = msg->getFlags();

        // preserve unread (do not archive any messages that have not yet been read)
        if(conf.preserve_unread && !(flags & vmime::net::message::FLAG_SEEN))
        {
            info("--preserve-unread set, mail #{} is not read yet, excluding.", i);
            continue;
        }

        vmime::shared_ptr<const vmime::header> mail_header = msg->getHeader();

        vmime::shared_ptr<const vmime::datetime> vdt_mail = mail_header->Date()->getValue<vmime::datetime>();

        date::year y{vdt_mail->getYear()};
        date::month m{static_cast<unsigned int>(vdt_mail->getMonth())};
        date::day d{static_cast<unsigned int>(vdt_mail->getDay())};

        std::chrono::system_clock::time_point tp_mail = date::sys_days(y / m / d);

        auto diff_days = std::chrono::duration_cast<date::days>(tp_today - tp_mail);

        bool b_archive_this_mail{false};
        // --all (archive all mails, without distinction)
        if(conf.all)
        {
            b_archive_this_mail = true;
            info("--all is set, mail #{} is included in archive.", i);
        }
        else
        {
            // old enough to be archived
            if(diff_days.count() >= conf.days)
            {
                b_archive_this_mail = true;

                // but exclusions active?
                if(!conf.include_flagged && (flags & vmime::net::message::FLAG_MARKED))
                {
                    info("--include-flagged not set, mail #{} is flagged, excluding.", i);
                    b_archive_this_mail = false;
                }

                if(conf.size > 0 && msg->getSize() < conf.size)
                {
                    info("--size set, mail #{} is {} bytes and bigger than {} bytes}, excluding.", i, msg->getSize(), conf.size);
                    b_archive_this_mail = false;
                }
            }
            else
            {
                info("mail #{} is not old enough, excluded.", i);
            }
        }

        // so we checked all we should
        if(b_archive_this_mail)
        {
            archived_mail_count++;

            if(conf.delete_mail)
            {
                info("--delete is set, so deleting instead of archiving mail #{}", i);
            }
            else
            {
                info("archiving mail #{}", i);

                std::stringstream sMessage;
                vmime::utility::outputStreamAdapter osa(sMessage);
                msg->extract(osa, nullptr, 0, -1, false);

                vmime::string s(sMessage.str());

                if(!conf.dont_mangle)
                {
                    replaceAll(s, "From", ">From");
                }
                else
                {
                    info("not replacing 'From' with '>From' since --no-mangle was set.");
                }

                // add clock to date
                tp_mail += std::chrono::hours(vdt_mail->getHour());
                tp_mail += std::chrono::minutes(vdt_mail->getMinute());
                tp_mail += std::chrono::seconds(vdt_mail->getSecond());

                // generate and add mail defining header row
                std::stringstream sts_prefix;
                sts_prefix << "From " << mail_header->From()->getValue()->generate() << " "
                           << date::format("%a %b %d %H:%M:%S %Y", std::chrono::time_point_cast<std::chrono::seconds>(tp_mail))
                           << std::endl;

                if(!conf.dry_run)
                {
                    os_mbox_target->write(sts_prefix.str().data(), sts_prefix.str().length());
                    os_mbox_target->write(s.data(), s.length());
                }
            }

            bool b_delete_mail{true};
            if(conf.copy || conf.dry_run)
            {
                b_delete_mail = false;
            }

            if(b_delete_mail)
            {
                info("flagging mail #{} to be deleted.", i);
                msg->setFlags(vmime::net::message::FLAG_DELETED , vmime::net::message::FLAG_MODE_ADD);
            }
        }
    }

    // a quick and dirty way to not expunge archived mails, when something failed during the process
    bool b_archiving_failed{false};

    // only if it was open
    if(os_mbox_target->is_open())
    {
        os_mbox_target->close();
        if(os_mbox_target->fail())
        {
            warning("writing mbox file failed: {}", strerror(errno));
            b_archiving_failed = true;
        }

        if(!conf.no_compress && !conf.delete_mail && archived_mail_count > 0)
        {
            // make sure to only write an archive if there is an export file (mbox)
            if(std::filesystem::exists(p_mbox_target))
            {
                std::filesystem::path p_archive{p_mbox_target};
                p_archive.replace_extension("gzip");
                info("writing archive file: {}", p_archive.string());

                // filename only, or we get the whole file path in the .zip file
                std::string f = p_mbox_target.filename();
                writeArchive(p_archive, p_mbox_target, f);

                if(std::filesystem::exists(p_archive) && std::filesystem::file_size(p_archive) > 0)
                {
                    if(std::filesystem::exists(p_mbox_target))
                    {
                        try
                        {
                            std::filesystem::remove(p_mbox_target);
                        }
                        catch(const std::exception& ex)
                        {
                            error("cannot delete mbox file {}, threw an exception: {}. ", p_mbox_target.string(), ex.what());
                            b_archiving_failed = true;
                        }
                    }
                }
            }
            else
            {
                error("can't write archive file, no mbox file found, although I just closed it.");
                b_archiving_failed = true;
            }
        }
        else
        {
            if(conf.no_compress)
            {
                info("not writing archive file, since --no-compress was set.");
            }
            if(conf.delete_mail)
            {
                info("not writing archive file, since --delete was set.");
            }
        }
    }

    // only open if it was opened before
    if(fld->isOpen())
    {
        // only delete flagged mails if everything ran as expected (and user wishes so)
        if(conf.copy || conf.dry_run || b_archiving_failed)
        {
            fld->close(false);
        }
        else
        {
            fld->close(true);
        }
    }

    // statistics
    if(!conf.quiet)
    {
        std::string s_action{};

        if(conf.dry_run)
        {
            s_action += "I would have ";
        }

        if(conf.delete_mail)
        {
            s_action += "deleted";
        }
        else
        {
            s_action += "archived";
        }

        // print "%s:\n    %s %d of %d message(s) (%s of %s) in %.1f seconds"
        warning("stats: {} {} of {} mail(s).", s_action, archived_mail_count, total_mail_count);
    }
}
// EOF