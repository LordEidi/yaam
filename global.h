/*-----------------------------------------------------------------------------
 **
 ** - yaam -
 **
 ** Yet Another ArchiveMail
 **
 ** Copyright 2023 by SwordLord - the coding crew - http://www.swordlord.com
 ** and contributing authors
 **
 ** This program is free software; you can redistribute it and/or modify it
 ** under the terms of the GNU Affero General Public License as published by the
 ** Free Software Foundation, either version 3 of the License, or (at your option)
 ** any later version.
 **
 ** This program is distributed in the hope that it will be useful, but WITHOUT
 ** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 ** FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 ** for more details.
 **
 ** You should have received a copy of the GNU Affero General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 **-----------------------------------------------------------------------------
 **
 ** Original Authors:
 ** LordEidi@swordlord.com
 **
-----------------------------------------------------------------------------*/
#ifndef YAAM_GLOBAL_H
#define YAAM_GLOBAL_H

#include <ctime>

#include <vmime/vmime.hpp>
#include <vmime/platforms/posix/posixHandler.hpp>

const std::string version{"v0.0.2a"};

// program args are put into these
struct Configuration {
    uint16_t                    days{180};
    std::string                 date_as_string{};
    std::string                 output_dir{};
    std::string                 pwfile{}; // path to file containing IMAP pwd
    std::string                 imap_pwd{}; // contains password, either from CLI or from file
    std::string                 filter_append{}; // IMAP: unused for now
    std::string                 prefix{};
    std::string                 prefix_formatted{};
    std::string                 suffix{"_archive"};
    std::string                 suffix_formatted{};
    std::string                 archive_name{};
    std::string                 archive_name_formatted{};
    uint16_t                    size{0};
    bool                        dry_run{false};
    bool                        preserve_unread{false};
    bool                        dont_mangle{false};
    bool                        delete_mail{false};
    bool                        copy{false};
    bool                        all{false};
    bool                        include_flagged{false};
    bool                        no_compress{false};
    bool                        warn_duplicate{false};
    bool                        verbose{false};
    bool                        debug_imap{false};
    bool                        quiet{false};
    bool                        version{false};
    bool                        no_tls_warning{false}; // basically ignores everything and says the CERT is OK
    std::vector<std::string>    mailbox{};
};

#endif //YAAM_GLOBAL_H
