/*-----------------------------------------------------------------------------
 **
 ** - yaam -
 **
 ** Yet Another ArchiveMail
 **
 ** Copyright 2023 by SwordLord - the coding crew - http://www.swordlord.com
 ** and contributing authors
 **
 ** This program is free software; you can redistribute it and/or modify it
 ** under the terms of the GNU Affero General Public License as published by the
 ** Free Software Foundation, either version 3 of the License, or (at your option)
 ** any later version.
 **
 ** This program is distributed in the hope that it will be useful, but WITHOUT
 ** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 ** FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 ** for more details.
 **
 ** You should have received a copy of the GNU Affero General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 **-----------------------------------------------------------------------------
 **
 ** Original Authors:
 ** LordEidi@swordlord.com
 **
-----------------------------------------------------------------------------*/

#include <string>

#include "CLI11.hpp"
#include "date.h"
#include "global.h"
#include "helper.h"
#include "archiver.h"

#include "log.h"

vmime::shared_ptr<vmime::net::session> sess = nullptr;
Configuration conf{};

// How
// archivemail --days=60 $dry_run --output-dir=$output_dir --suffix=$output_suffix $directory_to_backup
//directory_to_backup='Maildir/.Trash/'
//output_dir='/home/xy/backups/'
//output_suffix='%d%m%Y_backup'

int main(int argc, char **argv)
{
    CLI::App app("Yet Another ArchiveMail - YAAM");

    // Timeframe, either date or days
    CLI::Option *oDays = app.add_option("-d,--days", conf.days, "Archive messages older than NUM days. The default is 180."); // cant exclude directly before constructed
    CLI::Option *oDate = app.add_option("-D,--date", conf.date_as_string, "Archive messages older than DATE.")->excludes(oDays);
    oDays->excludes(oDate);

    // TODO
    // Missing: -F STRING, --filter-append=STRING, Append STRING to the IMAP filter string. For IMAP wizards.
    // MISSING: app.add_flag("--warn-duplicate", conf.warn_duplicate, "Warn about duplicate Message-IDs that appear in the input mailbox.");

    app.add_option("-o,--output-dir", conf.output_dir, "Use the directory name PATH to store the mailbox archives.");
    app.add_option("-P,--pwfile", conf.pwfile, "Read IMAP password from file FILE instead of from the command line.");
    app.add_option("-p,--prefix", conf.prefix, "Prefix NAME to the archive name. NAME is expanded by date::format().");
    app.add_option("-s,--suffix", conf.suffix, "Use the suffix NAME to create the filename used for archives. The default is _archive.");
    app.add_option("-a,--archive-name", conf.archive_name, "Use NAME as the archive name. Default is name of mailbox.");
    app.add_option("-S,--size", conf.size, "Only archive messages that are NUM bytes or greater.");

    app.add_flag("-n,--dry-run", conf.dry_run, "Don't write to any files, just show what would have been done.")->configurable(false);
    app.add_flag("-u,--preserve-unread", conf.preserve_unread, "Do not archive any messages that have not yet been read.");
    app.add_flag("--dont-mangle", conf.dont_mangle, "Do not mangle lines in message bodies beginning with “From ”.");
    app.add_flag("--delete", conf.delete_mail, "Delete rather than archive old mail. Use this option with caution!");
    app.add_flag("--copy", conf.copy, "Copy rather than archive old mail. This is a complement to the --delete option.");
    app.add_flag("--all", conf.all, "Archive all messages, without distinction.");
    app.add_flag("--include-flagged", conf.include_flagged, "Messages that are flagged important are not archived or deleted by default.");
    app.add_flag("--no-compress", conf.no_compress, "Do not compress any archives.");
    app.add_flag("--no-tls-warning", conf.no_tls_warning, "Accept all TLS certififcates without checking.");
    app.add_flag("--debug-imap", conf.debug_imap, "Activate IMAP debugging.");

    // either verbose or quiet (or none)
    CLI::Option *oVerbose = app.add_flag("-v, --verbose", conf.verbose, "Reports lots of extra debugging information about what is going on.");
    CLI::Option *oQuiet = app.add_flag("-q, --quiet", conf.quiet, "Turns on quiet mode.")->excludes(oVerbose);
    oVerbose->excludes(oQuiet);

    app.add_flag("-V, --version", conf.version, "Display the version of YAAM and exit.");

    app.add_option<std::vector<std::string>>("mailbox",conf.mailbox, "mailboxes");

    app.positionals_at_end();

    app.get_config_formatter_base()->quoteCharacter('"', '"');

    CLI11_PARSE(app, argc, argv);

    // show version info and quit
    if(conf.version)
    {
        // make sure to see something
        conf.verbose = true;

        info(" __  __     ______     ______     __    __    ");
        info(R"(/\ \_\ \   /\  __ \   /\  __ \   /\ "-./  \   )");
        info(R"(\ \____ \  \ \  __ \  \ \  __ \  \ \ \-./\ \  )");
        info(R"( \/\_____\  \ \_\ \_\  \ \_\ \_\  \ \_\ \ \_\ )");
        info(R"(  \/_____/   \/_/\/_/   \/_/\/_/   \/_/  \/_/ )");

        info("Yet Another ArchiveMail - YAAM {}", version);
        info("(c) 2023 by SwordLord - the coding crew");
        return 0;
    }

    if(conf.mailbox.size() <= 0)
    {
        die("no mailbox given, nothing to archive.");
        return 0;
    }
    else if(conf.mailbox.size() > 1 && conf.archive_name.length() > 0)
    {
        die("you cant use --archive-name with multiple mailboxes.");
        return 0;
    }

    // convert date to days if need be
    if(oDate->as<bool>())
    {
        date::year_month_day ymdParsed{};

        std::istringstream is{conf.date_as_string};
        date::from_stream(is, "%Y-%m-%d", ymdParsed);
        if(!ymdParsed.ok())
        {
            std::istringstream is2{conf.date_as_string};
            date::from_stream(is2, "%d %b %Y", ymdParsed);
            if(!ymdParsed.ok())
            {
                die("cant parse --date param: {}", conf.date_as_string);
                return 0;
            }
        }

        info("--date parsed as {}.{}.{} (d.m.y).", ymdParsed.day(), ymdParsed.month(), ymdParsed.year());

        std::chrono::system_clock::time_point tpParsed = date::sys_days{ymdParsed};
        std::chrono::system_clock::time_point tpToday = floor<date::days>(std::chrono::system_clock::now());

        if(tpParsed > tpToday)
        {
            die("--date is in the future.");
            return 0;
        }

        auto diffDays = std::chrono::duration_cast<date::days>(tpToday - tpParsed);

        conf.days = diffDays.count();

        info("converting --date '{}' into '{}' days.", conf.date_as_string, conf.days);
    }

    // make sure to have absolute paths from user input
    std::filesystem::path p = std::filesystem::path(conf.output_dir);
    if(p.is_relative())
    {
        try
        {
            conf.output_dir = std::filesystem::canonical(p);
        }
        catch(const std::exception& ex)
        {
            die("parsing --output-dir for {} threw an exception: {}.", conf.output_dir, ex.what());
            return 0;
        }

        // TODO should we create the directory if not existing?
        //std::filesystem::create_directories(conf.output_dir);
    }

    if(conf.dry_run)
    {
        warning("running in dry_run mode, not changing anything.");
    }

    if(conf.no_tls_warning)
    {
        warning("accepting every certificate, this could be insecure depending on your scenario.");
    }

    std::chrono::system_clock::time_point tp_now = std::chrono::system_clock::now();
    if(conf.prefix.length() > 0)
    {
        conf.prefix_formatted = date::format(conf.prefix, std::chrono::time_point_cast<std::chrono::seconds>(tp_now));
    }

    if(conf.suffix.length() > 0)
    {
        conf.suffix_formatted = date::format(conf.suffix, std::chrono::time_point_cast<std::chrono::seconds>(tp_now));
    }

    if(conf.archive_name.length() > 0)
    {
        conf.archive_name_formatted = date::format(conf.archive_name, std::chrono::time_point_cast<std::chrono::seconds>(tp_now));
    }

    if(conf.pwfile.length() > 0)
    {
        if(!std::filesystem::exists(conf.pwfile))
        {
            warning("password file '{}' does not exist, nothing read", conf.pwfile);
        }
        else
        {
            conf.imap_pwd = readFile(conf.pwfile);
            info("password file '{}' read", conf.pwfile);
        }
    }

    // let the games begin
    sess = vmime::net::session::create();

    for (auto mb : conf.mailbox)
    {
        archiveMailsource(mb);
    }

    return 0;
}

//EOF