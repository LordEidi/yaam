/*-----------------------------------------------------------------------------
 **
 ** - yaam -
 **
 ** Yet Another ArchiveMail
 **
 ** Copyright 2023 by SwordLord - the coding crew - http://www.swordlord.com
 ** and contributing authors
 **
 ** This program is free software; you can redistribute it and/or modify it
 ** under the terms of the GNU Affero General Public License as published by the
 ** Free Software Foundation, either version 3 of the License, or (at your option)
 ** any later version.
 **
 ** This program is distributed in the hope that it will be useful, but WITHOUT
 ** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 ** FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 ** for more details.
 **
 ** You should have received a copy of the GNU Affero General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 **-----------------------------------------------------------------------------
 **
 ** Original Authors:
 ** LordEidi@swordlord.com
 **
-----------------------------------------------------------------------------*/

#ifndef YAAM_ARCHIVER_H
#define YAAM_ARCHIVER_H

#include <iostream>
#include <fstream>
#include <iomanip>
#include <filesystem>

#include "global.h"

#include <vmime/vmime.hpp>
#include <fmt/chrono.h>

extern vmime::shared_ptr<vmime::net::session> sess;
extern Configuration conf;

void archiveMailsource(std::string& ms);
void archiveMails(std::string& ms, std::string& target_mailbox_name, vmime::utility::url& url);

#endif //YAAM_ARCHIVER_H
