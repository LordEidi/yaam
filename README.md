# Yet Another ArchiveMail

[![status-badge](https://ci.codeberg.org/api/badges/LordEidi/yaam/status.svg)](https://ci.codeberg.org/LordEidi/yaam)

## What?

*YAAM* is a tool to archive mails. It lets you archive mails based on your parameters from a local Maildir or IMAP folder into a compressed mbox file. 


## Why?

There was that handy piece of script which was called *archivemail*. Unfortunately the last update was in 2011 and the script still uses Python 2 and has no active maintainer.

We used this script regularly and were not able to find a replacement. We ported *archivemail* to modern C++ so that we can start again to archive our mails exactly as we used to. 

## Status

We use the tool successfully on our own systems. But *YAAM* is still young and there probably are tons of bugs still hidden in there somewhere. If you find one, please send us a bug report or a patch to fix it. And please use *YAAM* with caution.

Most of the parameters are compatible with *archivemail* and should work exactly as expected when you used that script before.

## How?

### Install and use

To run *YAAM*, you will have to install these fine libraries:

````
sudo apt install libfmt7 libarchive13 libvmime1 libre2-9
````

Then head to our download page, get the binary of *YAAM* and place it somewhere on your system.

To run *YAAM*, use this example (--dry-run makes sure to not change anything. Remove it when you are ready):

````
./yaam --days=60 --dry-run --output-dir=/tmp ./Maildir/.mailinglist
````
or
````
./yaam --days=60 --dry-run --output-dir=/tmp --pwfile /path/to/file "imaps://user:@server/Inbox/your/folder"
````


For details, see:

````
./yaam --help
````


### Compile for yourself

````
sudo apt install libfmt-dev libarchive-dev libvmime-dev libre2-dev

cmake -DCMAKE_BUILD_TYPE=Release -S . -B build
cmake --build build --target yaam j 6
````

## Future plans

*YAAM* supports Maildirs and IMAP folders. Currently we are cleaning up the IMAP part. Further ideas welcome, as are pull requests. A Debian package, directly packaged here when tagging a release would be nice.

## Shoulders of Giants

*YAAM* makes use of the following software by different authors:

- VMime
- CLI11
- Date.h
- libarchive
- fmt
- RE2

## Licence
Copyright (C) 2023 SwordLord - the coding crew. Please see enclosed LICENCE file 
for details.

