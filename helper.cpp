/*-----------------------------------------------------------------------------
 **
 ** - yaam -
 **
 ** Yet Another ArchiveMail
 **
 ** Copyright 2023 by SwordLord - the coding crew - http://www.swordlord.com
 ** and contributing authors
 **
 ** This program is free software; you can redistribute it and/or modify it
 ** under the terms of the GNU Affero General Public License as published by the
 ** Free Software Foundation, either version 3 of the License, or (at your option)
 ** any later version.
 **
 ** This program is distributed in the hope that it will be useful, but WITHOUT
 ** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 ** FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 ** for more details.
 **
 ** You should have received a copy of the GNU Affero General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 **-----------------------------------------------------------------------------
 **
 ** Original Authors:
 ** LordEidi@swordlord.com
 **
-----------------------------------------------------------------------------*/

#include "fstream"
#include "helper.h"
#include <iostream>

auto readFile(std::string_view path) -> std::string
{
    constexpr auto read_size = std::size_t{4096};
    auto stream = std::ifstream{path.data()};
    stream.exceptions(std::ios_base::badbit);

    auto out = std::string{};
    auto buf = std::string(read_size, '\0');
    while (stream.read(& buf[0], read_size))
    {
        out.append(buf, 0, stream.gcount());
    }
    out.append(buf, 0, stream.gcount());
    return out;
}

void replaceAll(std::string& input, const std::string& from, const std::string& to)
{
    size_t pos = 0;
    while ((pos = input.find(from, pos)) != std::string::npos)
    {
        input.replace(pos, from.size(), to);
        pos += to.size();
    }
}

void writeArchive(std::filesystem::path& out, std::filesystem::path& in, std::string& filename)
{
    struct archive* a;
    struct archive_entry* entry;
    struct stat st{};
    char buff[8192];
    int len;
    int fd;

    a = archive_write_new();
    archive_write_set_format_7zip(a);
    archive_write_open_filename(a, out.c_str());

    std::string s_in = std::filesystem::canonical(in).c_str();

    stat(s_in.c_str(), &st);
    entry = archive_entry_new();
    archive_entry_set_pathname(entry, filename.c_str());
    archive_entry_set_size(entry, st.st_size);
    archive_entry_set_filetype(entry, AE_IFREG); //S_IFREG
    archive_entry_set_perm(entry, 0644);
    int r = archive_write_header(a, entry);
    if (r < ARCHIVE_OK || r == ARCHIVE_FATAL) {
        return;
    }
    fd = open(s_in.c_str(), O_RDONLY);
    len = read(fd, buff, sizeof(buff));
    while ( len > 0 ) {
        archive_write_data(a, buff, len);
        len = read(fd, buff, sizeof(buff));
    }
    close(fd);
    archive_entry_free(entry);

    archive_write_close(a);
    archive_write_free(a);
}


// EOF